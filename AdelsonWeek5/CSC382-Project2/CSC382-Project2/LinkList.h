/*
	Sam Adelson
	1/23/15

	Linked List Header File

*/

#include<fstream>
#include<iostream>
#include<string>
#include "Node.h"

using namespace std;



class List
{
public:
	List();

	bool isEmpty() {return head == NULL;}
	Node* InsertNode(int index, int value);
	Node* AppendNode(int value);
	int FindNode(int value);
	int DeleteNode(int value);

	int Maximum();
	int Minimum();

	Node* head;
	Node* tail;

	int ImportSort(string file, int sort);
	void BubbleSort();
	void CocktailSort();
	
};