/*
Sam Adelson
2/8/15

Data Import & Sort Header File

*/

#include"LinkList.h"
#include<string>
#include<fstream>

using namespace std;

void ImportSort(string file, Node* startNode, int sortNum);

void BubbleSort(Node* startNode);

void SelectSort(Node* startNode);
