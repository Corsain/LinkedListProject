/*
Sam Adelson
2/8/15

Data Import & Sort Source File

*/

#include"InputSorts.h"

void ImportSort(string file, Node* startNode, int sortNum)
{
	Node * currentNode = startNode;

	ifstream importFile;
	//int counter = 0;
	int temp;

	importFile.open(file);
	while (importFile >> temp)
	{
		Node * tempNode = new Node;

		tempNode->back = currentNode;
		tempNode->data = temp;
		tempNode->next = NULL;

	}


	if (sortNum == 1)
	{
		BubbleSort(startNode);
	}

	else if (sortNum == 2)
	{
		SelectSort(startNode);
	}

	else
	{

	}


}

void BubbleSort(Node* startNode)
{
	int temp1;
	int temp2;
	bool sorted = true;

	Node* current = startNode;

	do
	{
		while (current->next)
		{
			if (current->data > current->next->data)
			{
				sorted = false;
				temp1 = current->data;
				temp2 = current->next->data;

				current->data = temp2;
				current->next->data = temp1;
			}

			current = current->next;
		}

	} while (!sorted);

}

void SelectSort(Node startNode)
{

}