/*
	Sam Adelson
	1/23/15

	Linked List Source File

*/

#include<stdio.h>
#include<time.h>
#include "LinkList.h"
//#include<ctime>

List::List()
{
	head = new Node;
	tail = new Node;

	head->back = NULL;
	head->next = tail;
	head->data = NULL;

	tail->next = NULL;
	tail->back = head;
	tail->data = NULL;

}

Node* List::InsertNode(int index, int value)
{
	if (index < 0)
	{
		return NULL;
	}

	int currentIndex = 0;
	Node* currentNode = head;
	while (currentNode && index > currentIndex)
	{
		currentNode = currentNode->next;

		currentIndex++;
	}

	if (index > 0 && currentNode == NULL)
	{
		return NULL;
	}


	
	if (index == 0)
	{
		Node* newNode = new Node;
		newNode->data = value;
		newNode->back = NULL;
		newNode->next = head;

		head->back = newNode;

		head = newNode;

		return newNode;
	}

	else
	{
		Node* newNode = new Node;

		newNode->next = currentNode->next;
		currentNode->next = newNode;
		newNode->data = value;
		return newNode;
	}

	
}



Node* List::AppendNode(int value)
{
	
	Node* prevNode = NULL;
	Node* currNode = head;
	
	while(currNode->next)
	{
		prevNode = currNode;
		currNode = currNode->next;

	}

	Node* newNode = new Node(value, currNode->next, currNode);


	return newNode;
}


int List::FindNode(int value)
{
	Node* currentNode = head;
	int currentIndex = 1;

	while (currentNode && currentNode->data != value)
	{
		//cout << "loopsy" << endl;

		currentNode = currentNode->next;
		currentIndex ++;
	}

	if (currentNode)
	{
		return currentIndex;
	}

	else
	{
		return 0;
	}
}


int List::DeleteNode(int value)
{
	Node* prevNode = NULL;
	Node* currNode = head;
	int currIndex = 0;

	while (currNode && currNode->data != value)
	{
		prevNode = currNode;
		currNode = currNode->next;
		currIndex++;
	}

	if (currNode)
	{
		if (prevNode)
		{
			prevNode->next = currNode->next;
			delete currNode;
		}

		else
		{
			head = currNode->next;
			delete currNode;
		}

		return currIndex;
	}

	return 0;
}

int List::Maximum()
{
	Node* currNode = head;
	int highVal = 0;
	int tempVal;

	while (currNode)

	{
		cout << endl << currNode->data;

		tempVal = currNode->data;
		
		if (highVal < tempVal)
		{
			highVal = tempVal;
		}
		currNode = currNode->next;

	}
	cout << endl;

	return highVal;
}

int List::Minimum()
{
	Node* currNode = head;
	int lowVal = 99999;

	while (currNode)
	{
		if (currNode->data < lowVal)
		{
			lowVal = currNode->data;
		}

		currNode = currNode->next;
	}

	return lowVal;
}


int List::ImportSort(string file, int sortNum)
{
	clock_t t;

	Node * currentNode = head;

	ifstream importFile;
	//int counter = 0;
	int temp = 0;
	cout << endl;

	importFile.open(file);
	while (importFile >> temp)
	{
		//cout << temp << endl;
		Node * tempNode = new Node(temp, currentNode->next, currentNode);

	}

	importFile.close();

	t = clock(); //Get clock time right before sort starts

	if (sortNum == 1)
	{
		//t = clock();

		BubbleSort();
	}

	else if (sortNum == 2)
	{
		//t = clock();

		CocktailSort();
	}

	else
	{
		cout << endl << "That isn't a valid choice." << endl;
	}

	t = clock() - t; //Get how much time has passed since sorts

	//cout << endl << "It took " << t << endl;
	printf("It took me %d clicks (%f seconds).\n", t, ((float)t) / CLOCKS_PER_SEC); //Output the time in clicks and seconds

	return 0;

}

void List::BubbleSort()
{
	int temp1;
	int temp2;
	bool sorted = true;

	

	do
	{
		Node* current = head;

		sorted = true;

		while (current->next != NULL)
		{
			if (current->data > current->next->data)
			{
				sorted = false;
				temp1 = current->data;
				temp2 = current->next->data;

				current->data = temp2;
				current->next->data = temp1;
			}

			current = current->next;
		}
		//cout << endl << "boop";  //Check to see if it is looping
	} while (!sorted);

}

void List::CocktailSort()
{
	int temp1;
	int temp2;
	bool sorted = true;

	Node* current = head;

	do
	{
		sorted = true;

		while (current->next != NULL)
		{
			if (current->data > current->next->data)
			{
				sorted = false;
				temp1 = current->data;
				temp2 = current->next->data;

				current->data = temp2;
				current->next->data = temp1;
			}

			current = current->next;
		}

		while (current->back != NULL)
		{
			if (current->data < current->back->data)
			{
				sorted = false;
				temp1 = current->data;
				temp2 = current->back->data;

				current->data = temp2;
				current->back->data = temp1;
			}

			current = current->back;
		}
		//cout << endl << "boop";  //Check to see if it is looping
	} while (!sorted);

}