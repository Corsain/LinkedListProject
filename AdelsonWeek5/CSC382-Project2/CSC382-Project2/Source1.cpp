/*
Sam Adelson
2/8/15

Project 2 Main Source File

*/

#include "LinkList.h"

int main()
{
	List test = List();

	//Sorts with the different numbers (Only do one per run)

	//test.ImportSort("num10.dat", 1);  //Bubble Sort with 10 numbers

	//test.ImportSort("num10.dat", 2);  //Cocktail Sort with 10 numbers

	//test.ImportSort("num100.dat", 1);  //Bubble with 100 numbers

	//test.ImportSort("num100.dat", 2);  //Cocktail with 100 numbers

	//test.ImportSort("num1000.dat", 1);  //Bubble with 1000

	//test.ImportSort("num1000.dat", 2);  //Cocktail with 1000

	//test.ImportSort("num10000.dat", 1);  //Bubble 10,000

	//test.ImportSort("num10000.dat", 2);  //Cocktail 10,000

	//test.ImportSort("num100000.dat", 1);  //Bubble 100,000

	//test.ImportSort("num100000.dat", 2);  //Cocktail 100,000

	//test.ImportSort("num1000000.dat", 1);  //Bubble 1,000,000

	//test.ImportSort("num1000000.dat", 2);  //Cocktail 1,000,000

/*	
// Output entire sorted list (DO NOT DO FOR GREATER THAN 10 NUMBERS!!)
Node * thing = test.head;
	
	while (thing)
	{
		cout << thing->data << endl;

		thing = thing->next;
	}

	//cout << endl << test.head->next->next->data;*/

	system("Pause"); //Pause the screen to see results
}

