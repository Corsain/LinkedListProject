/*
	Sam Adelson
	1/23/15

	Node Source File

*/

#include"Node.h"
using namespace std;

Node::Node()
{
	int value;
	next = NULL;
	back = NULL;
}

Node::Node(int value, Node* up, Node* down) : data(value), next(up), back(down)
{

	if (up)
	{ 
		//cout << endl << "boop";  check to see if statement runs
		up->back = this;
	}

	if (down)
	{
		//cout << endl << "boop";
		down->next = this;
	}
	
}

void Node::SetData(int value)
{
	data = value;
}

Node* Node::GetNext()
{
	return next;
}