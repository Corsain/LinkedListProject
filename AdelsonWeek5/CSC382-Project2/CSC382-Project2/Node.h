/*
	Sam Adelson
	1/23/15

	Node Header File

*/

#include <cstring>
#include <iostream>

class Node
{
public:
	Node();
	Node(int value, Node* up, Node* down);

	void SetData(int val);

	Node* GetNext();

	int data;
	Node* next;
	Node* back;
};
